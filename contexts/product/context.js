import React, { createContext, useState, useContext, useEffect, useReducer } from 'react'
import Cookies from 'js-cookie'
import productReducer from './reducer';
import { useRouter } from "next/router";
import { useQuery } from "react-query"; 
import { fetchProduct } from "../../services/product/get-product"

const CurrentProductContext = createContext({});

export const CurrentProductProvider = ({ children }) => {
  const { data } = useQuery('posts', fetchProduct, {staleTime: 10000});
  const [state, dispatch] = React.useReducer(
    productReducer,
    data
  );

  const getProductName = (state) => {
    return state.name;
  }

  const getProductInfo = () => {
    return state.name;
  }

  const value = React.useMemo(
    () => ({
      state,
      getProductName,
      getProductInfo
    }),
    [state]
  );

    return (
    <CurrentProductContext.Provider value={value}>
      {children}
    </CurrentProductContext.Provider>
  )
}



export const useCurrentProduct = () => useContext(CurrentProductContext)
