// import { search } from "../../framework/basic-rest/product/get-product";
// import { API_ENDPOINTS } from "../../framework/utils/api-endpoints";

import {API_ENDPOINTS} from "../../services/utils/endpoint"
import { fetchProduct, fetchProductServer } from "../../services/product/get-product"
import { QueryClient } from 'react-query'
import { dehydrate } from 'react-query/hydration'
import ProductDetail from "../../components/product-detail"
import { useQuery } from "react-query";
import { CurrentProductProvider } from "../../contexts/product/context"

export default function Product({}) {
	return (
		<CurrentProductProvider>
      <ProductDetail/>
		</CurrentProductProvider>
	);
}

export async function getServerSideProps() {
	const queryClient = new QueryClient()
 
	await queryClient.prefetchQuery('posts', fetchProduct)
	return {
		props: {
			dehydratedState: JSON.parse(JSON.stringify(dehydrate(queryClient)))
		},
	}
}

