// import { Product } from "../types";
import http from "../utils/http";
import { API_ENDPOINTS } from "../utils/endpoint";
import { useQuery } from "react-query";

export const fetchProduct = async () => {
	const { data } = await http.get(`http://localhost:3000/product.json`);
	return data;
};

export const useProductQuery = (slug) => {
	return useQuery('getProduct', 
		fetchProduct
	);
};
